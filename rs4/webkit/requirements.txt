# ubuntu -----------------------------------
# wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
# echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
# sudo apt update && sudo apt install -y google-chrome-stable
# sudo pip3 install -Ur requirements.txt

# centos -----------------------------------
# wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
# yum localinstall google-chrome-stable_current_x86_64.rpm
# google-chrome --version
# yum install chromedriver

chromedriver-autoinstaller
selenium
lxml
cssselect
html5lib
