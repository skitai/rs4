Installation

```bash
sudo apt-get install portaudio19-dev libffi-dev libssl-dev llvm-dev sox
pip3 install -Ur requirements.txt
```

1. [Project](https://console.actions.google.com/)

- Create project and devices
- Save OAuth credential

2. [Google APIs](https://console.developers.google.com/apis/library/embeddedassistant.googleapis.com/)

    - Enable GA API

3. [Activity Control ](https://myaccount.google.com/u/2/activitycontrols?pageId=none)

    - Enable `Web and App Activities > Including Voice and Audio`

4. Register Device


```bash
google-oauthlib-tool --scope https://www.googleapis.com/auth/assistant-sdk-prototype --save --headless --client-secrets ~/client_secret_***.json
```

File created `/home/pi/.config/google-oauthlib-tool/credentials.json`
