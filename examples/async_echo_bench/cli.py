from socket import *
import time
import sys

def benchmark (addr, nmessages):
    sock = socket (AF_INET, SOCK_STREAM)
    sock.connect (addr)
    start  = time.time ()
    for n in range (nmessages):
        sock.send (b'x')
        resp = sock.recv (10000)
        if nmessages < 100:
            print (resp)        
    end  = time.time ()    
    print (nmessages / (end-start), "messages/sec")

benchmark (("localhost", 25000), int (sys.argv [1]))

"""
100000 times results on Pythoin 3.5 (3 rties) ------------------------------- 
 - asyncio 12597, 14266, 13417
 - gevent: 32779, 34273, 34789
 - Beazley hack: 42312, 40011,  39751
 - asyncore 49159, 50023, 50326  
"""
