"""
Hans Roh 2015 -- http://osp.skitai.com
License: BSD
"""
import re
import sys
import os
import shutil, glob
from warnings import warn
try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

with open('rs4/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*"(.*?)"',fd.read(), re.M).group(1)

if 'publish' in sys.argv:
	os.system ('{} setup.py bdist_wheel'.format (sys.executable))
	whl = glob.glob ('dist/rs4-{}-*.whl'.format (version))[0]
	os.system ('twine upload {}'.format (whl))
	os.system ('pip3 install -U --pre rs4==1000 > /dev/null 2>&1')
	sys.exit ()

classifiers = [
  'License :: OSI Approved :: MIT License',
  'Development Status :: 4 - Beta',
  'Topic :: Utilities',
	'Environment :: Console',
	'Topic :: Software Development :: Libraries :: Python Modules',
	'Intended Audience :: Developers',
	'Programming Language :: Python',
	'Programming Language :: Python :: 3'
]

PY_MAJOR_VERSION = sys.version_info [0]
if PY_MAJOR_VERSION == 3:
	if os.path.isfile ("rs4/lib/py2utils.py"):
		os.remove ("rs4/lib/py2utils.py")
		try: os.remove ("rs4/lib/py2utils.pyc")
		except OSError: pass
else:
	if not os.path.isfile ("rs4/lib/py2utils.py"):
		with open ("rs4/lib/py2utils.py", "w") as f:
			f.write ("def reraise(type, value, tb):\n\traise type, value, tb\n")

packages = [
	'rs4',
	'rs4.psutil',
	'rs4.webkit',
	'rs4.apis',
	'rs4.apis.shared',
	'rs4.apis.gitlab',
	'rs4.apis.aws',
	'rs4.apis.aws.ec2ops',
	'rs4.apis.google',
	'rs4.apis.google.assistant',
	'rs4.apis.google.assistant.grpc',
	'rs4.apis.etri',
	'rs4.apis.houndify',
	'rs4.apis.snowboy',
	'rs4.apis.mycroft',
	'rs4.apis.mycroft.precise',
	'rs4.nets',
	'rs4.nets.ipsec',
	'rs4.nets.proxy',
	'rs4.nets.ssh',
	'rs4.nets.ssh.commands',
	'rs4.misc',
	'rs4.misc.installer',
	'rs4.misc.hyperopt',
	'rs4.misc.hyperopt.utils',
	'rs4.misc.hyperopt.spaces',
	'rs4.misc.hyperopt.strategies',
	'rs4.misc.hyperopt.strategies.pbt',
	'rs4.protocols',
	'rs4.protocols.aquests',
    'rs4.protocols.sock',
    'rs4.protocols.sock.impl',
	'rs4.protocols.sock.impl.dns',
	'rs4.protocols.sock.impl.dns.pydns',
	'rs4.protocols.sock.impl.http',
	'rs4.protocols.sock.impl.http2',
	'rs4.protocols.sock.impl.http2.hyper',
	'rs4.protocols.sock.impl.http2.hyper.common',
	'rs4.protocols.sock.impl.http2.hyper.http11',
	'rs4.protocols.sock.impl.http2.hyper.http20',
	'rs4.protocols.sock.impl.http2.hyper.http20.h2',
	'rs4.protocols.sock.impl.http2.hyper.packages',
	'rs4.protocols.sock.impl.http2.hyper.packages.rfc3986',
    'rs4.protocols.sock.impl.http3',
	'rs4.protocols.sock.impl.ws',
	'rs4.protocols.sock.impl.smtp',
	'rs4.protocols.sock.impl.grpc',
	'rs4.protocols.sock.impl.proxy',
]

package_dir = {'rs4': 'rs4'}

package_data = {
	"rs4": [
		"apis/google/*.txt",
		"apis/google/assistant/*.txt",
		"protocols/sock/impl/dns/*.txt",
		"protocols/sock/impl/dns/pydns/*.txt",
		"protocols/sock/impl/http2/hyper/*.txt",
		"protocols/sock/impl/http2/hyper/*.pem",
        "protocols/sock/impl/http3/*.pem",
	]
}

install_requires = [
	"psutil",
	"requests",
	"colorama",
	"tqdm",
	"setproctitle"
]

if os.name == "posix":
	install_requires.append ("setproctitle")


if __name__ == "__main__":
	with open ('README.rst', encoding='utf-8') as f:
		long_description = f.read()

	setup(
		name='rs4',
		version=version,
		description='Utility Pack',
		long_description = long_description,
		url = 'https://gitlab.com/skitai/rs4',
		author='Hans Roh',
		author_email='hansroh@gmail.com',
		packages=packages,
		package_dir=package_dir,
		package_data = package_data,
		license='MIT',
		platforms = ["posix", "nt"],
		download_url = "https://pypi.python.org/pypi/rs4",
		install_requires = install_requires,
		classifiers=classifiers
	)
